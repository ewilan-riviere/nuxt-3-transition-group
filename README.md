# Nuxt 3 Transition Group

## Setup

Make sure to install the dependencies:

```bash
pnpm i
```

## Development Server

Start the development server on <http://localhost:3000>

```bash
pnpm dev
```
